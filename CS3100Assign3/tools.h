/*Brad Richardson
remade so it can be imported anywhere*/
#include <vector>
#include <list>
#include <array>
#include <deque>
#include <math.h> //pow and sqrt
#include <numeric> //accumulate
#include <ctime> //timing function, srand seed
#include <algorithm> //sort, generate, blah
#include <iostream>

//takes a std container and returns the mean
template<typename T>
double mean(T container)
{
	//need to have a double (eg 0.0) or else will truncate decimals for summation
	auto sum = std::accumulate(container.begin(), container.end(), 0.0);
	return sum / container.size();
}

//takes a std container and a mean and returns the std deviation of each element in the same type container
template<typename T>
double stdDev(T container)
{
	if (container.size() <= 1)
		return 0;
	auto meanVal = mean(container);
	std::vector<double> newvals(container.size());
	std::transform(container.begin(), container.end(), newvals.begin(), [&](double val){return std::pow((val - meanVal), 2); });
	return std::sqrt(std::accumulate(newvals.begin(), newvals.end(), 0.0) / (newvals.size() - 1));
}

//takes a function and times it, then returns the time it took to run in seconds
template<typename F>
double functionTimer(F function)
{
	auto start = std::clock();
	function();
	return (std::clock() - start) / (double) CLOCKS_PER_SEC;
}

//using the vector from functionTimerAvg(), prints out mean and stdDev
void printInfo(std::vector<double> container)
{
	std::cout << "Mean runtime: " << container.at(0) << std::endl;
	std::cout << "Standard deviation of runtime: " << container.at(1) << std::endl;
}

//runs a function four times and then returns the mean and std deviation in a vector
template<typename F>
std::vector<double> functionTimerAvg(F function, int timesToRun = 3)
{
	std::vector<double> times(timesToRun);
	std::generate(times.begin(), times.end(), [&](){return functionTimer(function); });
	std::vector<double> toReturn = { mean(times), stdDev(times) };
	return toReturn;
}