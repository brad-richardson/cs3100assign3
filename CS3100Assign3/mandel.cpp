/*Brad Richardson A01240240 CS3100 Assign 3*/
#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <thread>
#include <mutex>
#include "tools.h"

const int MAXITERS = 1000;
int XPIXELS = 640;
int YPIXELS = 480;

int getRed(int);
int getGreen(int);
int getBlue(int);
int getMbrotCount(double, double);

//receives the scaled pixel from mandelbrot scale and returns color to be used
std::vector<int> getPixel(double xPixel, double yPixel)
{
	auto iters = getMbrotCount(xPixel, yPixel);
	std::vector<int> pixelColors = { getRed(iters), getGreen(iters), getBlue(iters) };
	return pixelColors;
}

//generates pixels from one row of pixels at a time and writes to given 2d vector
void mandelThread(int &curr_row, std::vector<std::vector<int>> &pixels, std::mutex &row_mutex)
{
	auto x1 = -2.5;
	auto y1 = -1.0;
	auto x2 = 1.0;
	auto y2 = 1.0;

	while (true)
	{
		int row = 0;
		//allows for incrementing without having bad values between threads
		{
			std::lock_guard<std::mutex> guard(row_mutex);
			row = curr_row++;
		}
		if (row >= YPIXELS) return;

		auto y = y1 + ((y2 - y1) / YPIXELS)*row;
		for (auto i = 0; i < XPIXELS; ++i)
		{
			auto x = x1 + ((x2 - x1) / XPIXELS)*i;
			auto pixelColors = getPixel(x, y);
			pixels[row][i * 3] = pixelColors[0];
			pixels[row][i * 3 + 1] = pixelColors[1];
			pixels[row][i * 3 + 2] = pixelColors[2];
		}
	}
}

//make the mandelbrot image using the given pixel dimensions
void makeMandel(int threadCount)
{
	std::ofstream outfile("mandelbrot.ppm");
	outfile << "P3" << std::endl << XPIXELS << " " << YPIXELS << std::endl << "255" << std::endl;
	//vectors to store threads and individual pixels
	std::vector <std::thread> threads;
	std::vector <std::vector<int>> output(YPIXELS, std::vector<int>(XPIXELS * 3));
	//need to make a row mutex to pass into the function, make that a infinite loop and lock down the row
	std::mutex row_mutex;
	auto row = 0;
	for (auto i = 0; i < threadCount; ++i)
	{
		//passes the vector around so that each thread can write to its own section
		threads.emplace_back(std::thread([&](){mandelThread(row, output, row_mutex);}));
	}
	for (auto i = 0; i < threadCount; ++i)
		threads[i].join();
	//takes pixels generated and outputs to file
	for (auto i = 0; i < YPIXELS; ++i)
	{
		for (auto j = 0; j < XPIXELS * 3; ++j)
		{
			outfile << output[i][j] << " ";
		}
		outfile << std::endl;
	}
	outfile << std::endl;
	outfile.close();
	return;
}

int main(int argc, char* argv[])
{
	int resolution;
	int threadCount;
	if (argc < 3)
	{
		std::cout << "What resolution image to generate? (320, 480, 720 or 1080) ";
		std::cin >> resolution;
		
		std::cout << "How many threads to run on? ";
		std::cin >> threadCount;
	}
	else
	{
		resolution = atoi(argv[1]);
		threadCount = atoi(argv[2]);
	}
	switch (resolution)
	{
	case 320:
	default:
		XPIXELS = 480;
		YPIXELS = 320;
		break;
	case 480:
		XPIXELS = 640;
		YPIXELS = 480;
		break;
	case 720:
		XPIXELS = 1280;
		YPIXELS = 720;
		break;
	case 1080:
		XPIXELS = 1920;
		YPIXELS = 1080;
		break;
	}
	std::cout << "Creating Mandelbrot set ppm...\n";
	auto timing = functionTimerAvg([&](){makeMandel(threadCount); });
	std::cout << "Finished!\n";
	printInfo(timing);
	return 0;
}

int getRed(int c){
	return 0 * (c % 4);
}
int getGreen(int c){
	return 10 * (c % 50);
}
int getBlue(int c){
	return 30 * (c % 70);
}

//returns iterations for current pixel
int getMbrotCount(double a0, double b0){
	int count = 0;
	double anext, bnext;
	double a, b;
	a = a0;
	b = b0;
	while (a*a + b*b <= 8.0 && count<MAXITERS){
		anext = a*a - b*b + a0;
		bnext = 2.0*a*b + b0;
		a = anext;
		b = bnext;
		++count;
	}
	return count;
}